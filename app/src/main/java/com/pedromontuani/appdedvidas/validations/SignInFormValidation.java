package com.pedromontuani.appdedvidas.validations;

import android.content.Context;
import android.content.res.Resources;
import android.widget.EditText;

import androidx.core.util.PatternsCompat;

import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.SignInFragmentBinding;

public class SignInFormValidation extends BaseValidation {
    private final EditText emailInput, passwordInput;

    public SignInFormValidation(Context context, SignInFragmentBinding binding) {
        super(context);

        this.emailInput = binding.inputEmail;
        this.passwordInput = binding.inputPassword;
        
        addEmailValidation();
        addPasswordValidation();
    }

    private void addEmailValidation() {
        this.awesomeValidation.addValidation(emailInput, PatternsCompat.EMAIL_ADDRESS, this.context.getString(R.string.invalid_email));
    }
    
    private void addPasswordValidation() {
        this.awesomeValidation.addValidation(passwordInput, RegexTemplate.NOT_EMPTY, this.context.getString(R.string.invalid_password));
    }
}

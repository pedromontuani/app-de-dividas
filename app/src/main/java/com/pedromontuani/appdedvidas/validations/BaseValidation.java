package com.pedromontuani.appdedvidas.validations;

import android.content.Context;
import android.content.res.Resources;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

public abstract class BaseValidation {
    protected final AwesomeValidation awesomeValidation;
    protected final Context context;

    public BaseValidation(Context context) {
        this.awesomeValidation = new AwesomeValidation(ValidationStyle.UNDERLABEL);
        this.awesomeValidation.setContext(context);
        this.context = context;
    }

    public boolean validate() {
        return this.awesomeValidation.validate();
    }
}

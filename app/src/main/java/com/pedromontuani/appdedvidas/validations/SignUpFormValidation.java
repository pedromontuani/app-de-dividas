package com.pedromontuani.appdedvidas.validations;

import android.content.Context;
import android.content.res.Resources;
import android.widget.EditText;

import androidx.core.util.PatternsCompat;

import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.SignUpFragmentBinding;

public class SignUpFormValidation extends BaseValidation{
    private final EditText nameInput, emailInput, passwordInput, confirmPasswordInput;

    public SignUpFormValidation(Context context, SignUpFragmentBinding binding) {
        super(context);
        this.nameInput = binding.signUpNameInput;
        this.emailInput = binding.signUpEmailInput;
        this.passwordInput = binding.signUpPasswordInput;
        this.confirmPasswordInput = binding.signUpPasswordConfirmInput;

        addNameValidation();
        addEmailValidation();
        addPasswordValidation();
        addConfirmPasswordInput();
    }

    private void addNameValidation () {
        this.awesomeValidation.addValidation(nameInput, RegexTemplate.NOT_EMPTY, this.context.getString(R.string.invalid_name));
    }

    private void addEmailValidation() {
        this.awesomeValidation.addValidation(emailInput, PatternsCompat.EMAIL_ADDRESS, this.context.getString(R.string.invalid_email));
    }

    private void addPasswordValidation() {
        this.awesomeValidation.addValidation(passwordInput, RegexTemplate.NOT_EMPTY, this.context.getString(R.string.invalid_new_password));
    }

    private void addConfirmPasswordInput() {
        this.awesomeValidation.addValidation(confirmPasswordInput, passwordInput, this.context.getString(R.string.passwords_dont_match));
    }

}

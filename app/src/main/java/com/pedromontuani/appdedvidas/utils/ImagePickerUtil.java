package com.pedromontuani.appdedvidas.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;

import androidx.fragment.app.Fragment;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;

public class ImagePickerUtil {
    public static void launchSingleImagePicker (Fragment fragment, Context context) {
        ImagePicker.create(fragment)
                .returnMode(ReturnMode.ALL)
                .folderMode(true)
                .single()
                .includeVideo(false)
                .showCamera(true)
                .start();
    }
}

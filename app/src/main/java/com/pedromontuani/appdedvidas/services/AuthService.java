package com.pedromontuani.appdedvidas.services;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pedromontuani.appdedvidas.models.User;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class AuthService {
    private final FirebaseAuth firebaseAuth;
    private final FirebaseFirestore firebaseFirestore;
    private final FirebaseStorage firebaseStorage;

    public AuthService() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
    }

    public boolean createUser(String name, String email, String password, Uri imageUri) {
        try {
            AuthResult authResult = Tasks.await(firebaseAuth.createUserWithEmailAndPassword(email, password));
            FirebaseUser user = authResult.getUser();


            Uri downloadImageUri = Objects.nonNull(imageUri) ? Tasks.await(uploadImage(imageUri, Objects.requireNonNull(user))) : null;

            Tasks.await(updateUserData(name, downloadImageUri, user));
            user.reload();

            User userData = new User();
            userData.setName(name);
            userData.setPhotoUrl(Objects.nonNull(downloadImageUri) ? downloadImageUri.toString() : null);

            Tasks.await(setFirestoreData(user.getUid(), userData));

            return true;
        } catch (ExecutionException | InterruptedException e) {
            Log.e("AuthService", "Error creating new user", e);
        }
        return false;
    }

    private Task<Uri> uploadImage(Uri imageUri, FirebaseUser user) throws ExecutionException, InterruptedException {
        StorageReference userImageRef = firebaseStorage.getReference().child("users").child(user.getUid()).child("profile_image.jpg");
        Tasks.await(userImageRef.putFile(imageUri));
        return userImageRef.getDownloadUrl();
    }

    private Task<Void> updateUserData(String name, Uri imageUri, FirebaseUser user) {
        UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
        builder.setDisplayName(name);
        builder.setPhotoUri(imageUri);
        UserProfileChangeRequest request = builder.build();
        return user.updateProfile(request);
    }

    private Task<Void> setFirestoreData(String uid, User userData) {
        return firebaseFirestore.collection("users").document(uid).set(userData);
    }
}

package com.pedromontuani.appdedvidas.screens;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.NewDividaSelectUserFragmentBinding;
import com.pedromontuani.appdedvidas.models.User;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class NewDividaSelectUserScreen extends Fragment {
    private static final String USERS_LIST = "USERS_LIST";
    private NewDividaSelectUserFragmentBinding binding;
    private ArrayList<User> usersList;
    private FirebaseFirestore firestore;
    private FirebaseUser currentUser;
    private ArrayAdapter<String> usersListAdapter;
    private ArrayList<User> selectedUsers;
    private NavController navController;


    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = NewDividaSelectUserFragmentBinding.inflate(inflater, container, false);
        firestore = FirebaseFirestore.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        selectedUsers = new ArrayList<>();
        navController = NavHostFragment.findNavController(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(Objects.nonNull(savedInstanceState)) {
            usersList = (ArrayList<User>) savedInstanceState.getSerializable(USERS_LIST);
            setArrayAdapter();
        } else if(Objects.nonNull(usersList)) {
            setArrayAdapter();
        } else {
            getUsersList();
        }
        binding.newDividaUsersList.setOnItemClickListener(this::onClickUser);
    }

    @Override
    public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(USERS_LIST, usersList);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void getUsersList() {
        firestore.collection("users")
                .whereNotEqualTo(FieldPath.documentId(), currentUser.getUid())
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<User> users = queryDocumentSnapshots.getDocuments().stream().map(documentSnapshot -> documentSnapshot.toObject(User.class)).collect(Collectors.toList());
                        usersList = new ArrayList<>(users);
                        setArrayAdapter();
                    }
        });
    }

    private void setArrayAdapter() {
        List<String> userNames = usersList.stream().map(User::getName).collect(Collectors.toList());
        usersListAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, userNames);
        binding.newDividaUsersList.setAdapter(usersListAdapter);
    }

    private void onClickUser(AdapterView<?> parent, View view, int position, long id) {
        selectedUsers.add(usersList.get(position));
        NewDividaSelectUserScreenDirections.ActionNewDividaSelectUserScreenToNewDividaSetNameScreen action = NewDividaSelectUserScreenDirections.actionNewDividaSelectUserScreenToNewDividaSetNameScreen(selectedUsers);
        navController.navigate(action);
    }
}
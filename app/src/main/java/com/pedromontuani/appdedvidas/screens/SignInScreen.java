package com.pedromontuani.appdedvidas.screens;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.SignInFragmentBinding;
import com.pedromontuani.appdedvidas.validations.SignInFormValidation;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.Executor;

public class SignInScreen extends Fragment {

    private SignInFragmentBinding binding;
    private FirebaseAuth firebaseAuth;
    private SignInFormValidation validation;
    private NavController navController;

    @Override
    public View onCreateView(
            @NotNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = SignInFragmentBinding.inflate(inflater, container, false);
        firebaseAuth = FirebaseAuth.getInstance();
        validation = new SignInFormValidation(getContext(), binding);
        navController = NavHostFragment.findNavController(this);
        if(Objects.nonNull(firebaseAuth.getCurrentUser())) {
            navController.navigate(R.id.action_SignInFragment_to_DividasContentFragment);
        }
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.setSignUpButtonText();
        binding.loginButton.setOnClickListener(this::signIn);
        binding.goToSignUpButton.setOnClickListener(this::onClickSignUpButton);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void setSignUpButtonText() {
        String text = getString(R.string.go_to_sign_up_button);
        binding.goToSignUpButton.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
    }

    private void signIn(View view) {
        if(validation.validate()) {
            setProgressBarVisibility(true);
            String email = binding.inputEmail.getText().toString();
            String password = binding.inputPassword.getText().toString();
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    navController.navigate(R.id.action_SignInFragment_to_DividasContentFragment);
                } else {
                    Toast.makeText(getContext(), getString(R.string.wrong_email_password), Toast.LENGTH_LONG).show();
                }
                setProgressBarVisibility(false);
            });
        }
    }

    private void setProgressBarVisibility(Boolean loading) {
        if(loading) {
            binding.loginButton.setText("");
            binding.loginButton.setClickable(false);
            binding.signInButtonLoading.setVisibility(View.VISIBLE);
        } else {
            binding.loginButton.setText(R.string.login_button);
            binding.loginButton.setClickable(true);
            binding.signInButtonLoading.setVisibility(View.GONE);
        }
    }

    private void onClickSignUpButton (View view) {
        navController.navigate(R.id.action_SignInFragment_to_SignUpFragment);
    }

}
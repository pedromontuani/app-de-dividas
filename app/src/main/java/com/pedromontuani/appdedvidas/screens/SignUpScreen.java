package com.pedromontuani.appdedvidas.screens;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.SignUpFragmentBinding;
import com.pedromontuani.appdedvidas.models.User;
import com.pedromontuani.appdedvidas.services.AuthService;
import com.pedromontuani.appdedvidas.validations.SignUpFormValidation;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class SignUpScreen extends Fragment {

    private SignUpFragmentBinding binding;
    private SignUpFormValidation validation;
    private ActivityResultLauncher<Intent> imagePickerResultLaucher;
    private NavController navHostFragment;
    private Image userImage;
    private AuthService authService;

    @Override
    public View onCreateView(
            @NotNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = SignUpFragmentBinding.inflate(inflater, container, false);
        validation = new SignUpFormValidation(getContext(), binding);
        imagePickerResultLaucher = this.getImagePickerResultLauncher();
        navHostFragment = NavHostFragment.findNavController(this);
        authService = new AuthService();
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSignUpButtonText();
        binding.signUpButton.setOnClickListener(this::onClickSignUpButton);
        binding.addPhotoButton.setOnClickListener(this::onClickSelectImage);
        binding.signUpUserImage.setOnClickListener(this::onClickSelectImage);
        binding.goToSignInScreen.setOnClickListener(this::onClickGoToSignInButton);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private ActivityResultLauncher<Intent> getImagePickerResultLauncher() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            userImage = ImagePicker.getFirstImageOrNull(result.getData());
            if(Objects.nonNull(userImage)) {
                binding.signUpUserImage.setImageURI(userImage.getUri());
            }
        });
    }


    private void onClickSelectImage(View view) {
        Intent intent = ImagePicker.create(this)
                .includeVideo(false)
                .single().getIntent(getContext());

        imagePickerResultLaucher.launch(intent);
    }

    private void onClickSignUpButton(View view) {
        if(validation.validate()) {
            setSignUpButtonLoading(false, null);

            String name = binding.signUpNameInput.getText().toString();
            String email = binding.signUpEmailInput.getText().toString();
            String password = binding.signUpPasswordInput.getText().toString();
            Uri imageUri = Objects.nonNull(userImage) ? userImage.getUri() : null;
            Handler responseHandler = new Handler();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (authService.createUser(name, email, password, imageUri)) {
                        navigateToDividasContent(responseHandler);
                    } else {
                        showSignUpError(responseHandler);
                    }
                    setSignUpButtonLoading(true, responseHandler);
                }
            }).start();

        }

    }

    private void setSignUpButtonLoading(Boolean loading, Handler handler) {
        if(!loading) {
            binding.signUpButton.setText("");
            binding.signUpButtonLoading.setVisibility(View.VISIBLE);
            binding.signUpButton.setClickable(false);
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    binding.signUpButton.setText(R.string.sign_up_button);
                    binding.signUpButton.setClickable(true);
                    binding.signUpButtonLoading.setVisibility(View.GONE);
                }
            });
        }
    }

    private void navigateToDividasContent(Handler handler) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                navHostFragment.navigate(R.id.action_SignUpFragment_to_DividasContentFragment);
            }
        });
    }

    private void showSignUpError(Handler handler) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), getString(R.string.default_error_message), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setSignUpButtonText() {
        String text = getString(R.string.go_to_sign_in_button);
        binding.goToSignInScreen.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
    }


    private void onClickGoToSignInButton(View view) {
        navHostFragment.popBackStack();
    }

}
package com.pedromontuani.appdedvidas.screens;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pedromontuani.appdedvidas.R;
import com.pedromontuani.appdedvidas.databinding.DividasFragmentBinding;

import org.jetbrains.annotations.NotNull;

public class DividasScreen extends Fragment {
    private DividasFragmentBinding binding;
    private NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        navController = NavHostFragment.findNavController(this);
        binding = DividasFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.addDividaFloatingButton.setOnClickListener(this::onClickAddDivida);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void onClickAddDivida(View view) {
        navController.navigate(R.id.action_dividasScreen_to_newDividaSelectUserScreen);
    }
}
package com.pedromontuani.appdedvidas.models;

import com.google.firebase.firestore.DocumentId;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {
    @DocumentId
    private String id;
    private String name;
    private String photoUrl;
}
